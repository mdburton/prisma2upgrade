# Prisma 2 upgrade script

This script is designed to assist in the upgrade process from prisma 1 -> prisma 2.

The best place to start your migration is from the official prisma documentation available in the [How to upgrade guide](https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/how-to-upgrade)

There are four main guides to follow as examples during your upgrade

[Old to new Nexus: Choose this guide if you're currently running Prisma 1 with GraphQL Nexus.](https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-nexus-prisma-to-nexus/)

[prisma-binding to Nexus: Choose this guide if you're currently running Prisma 1 with prisma-binding and want to upgrade to Nexus.](https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-prisma-binding-to-nexus)

[prisma-binding to SDL-first: Choose this guide if you're currently running Prisma 1 with prisma-binding and want to upgrade to an SDL-first GraphQL server.](https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-prisma-binding-to-sdl-first/)

[REST API: Choose this guide if you're currently running Prisma 1 using Prisma client 1 and are building a REST API.](https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-a-rest-api/)
