# Upgrade Script Prisma 1 -> Prisma 2 
# This script will run prisma migration tools in sequence in each repo
# speficied in a repos.txt file
# The commands below should be upgraded according to your update guide
# See the sugestions below

# Old to new Nexus: Choose this guide if you're currently running Prisma 1 with GraphQL Nexus.
# https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-nexus-prisma-to-nexus/

# prisma-binding to Nexus: Choose this guide if you're currently running Prisma 1 with prisma-binding and want to upgrade to Nexus.
# https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-prisma-binding-to-nexus/

# prisma-binding to SDL-first: Choose this guide if you're currently running Prisma 1 with prisma-binding and want to upgrade to an SDL-first GraphQL server.
# https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-prisma-binding-to-sdl-first/

# REST API: Choose this guide if you're currently running Prisma 1 using Prisma client 1 and are building a REST API.
# https://www.prisma.io/docs/guides/upgrade-guides/upgrade-from-prisma-1/upgrading-a-rest-api/

# this command works to trim whitespace from the repos.txt file
while IFS="" read -r repo || [ -n "$repo" ]
do
  printf '%s\n test' "$repo"
  # todo migrate into each repo
  # determine if repo has package-lock.json or yarn.lock
  # install prisma as dev dep "yarn add -D @prisma/cli"
  # update prisma to at least v1.3
  # introspect db
  # fix errors
  # open ticket and push changes to branch
done < repos.txt

